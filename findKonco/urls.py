from django.urls import path
from . import views

app_name = 'findKonco'

urlpatterns = [
    path('myfindkonco', views.findKonco, name='myfindkonco'),
    path('myformkonco/', views.formFindKonco, name='myformkonco'),
    path('mydelkonco/<id>/', views.delete_konco, name='mydelkonco'),
]