from django.test import TestCase, Client
from django.urls import resolve
from .views import findKonco, formFindKonco, delete_konco
from .forms import FormFindKonser

# Create your tests here.
class TestFindKonco (TestCase):
    def test_findKonco_is_exist(self):
        response = Client().get('/findkonco/')
        self.assertEqual(response.status_code,200)

    def test_findKonco_using_html_template(self):
        response = Client().get('/findkonco/')
        self.assertTemplateUsed(response, 'findKonco.html')

    def test_findKonco_using_index_func(self):
        found = resolve('/findkonco/')
        self.assertEqual(found.func, index)


class TestFormFindKonco (TestCase):
    def test_formFindKonco_is_exist(self):
        response = Client().get('/formfindkonco/')
        self.assertEqual(response.status_code,200)

    def test_formFindKonco_using_html_template(self):
        response = Client().get('/formfindkonco/')
        self.assertTemplateUsed(response, 'formFindKonco.html')

    def test_formFindKonco_using_index_func(self):
        found = resolve('/formfindkonco/')
        self.assertEqual(found.func, index) 
