from . import models
from django import forms


class FormFindKonser(forms.ModelForm):
    namalengkap = forms.CharField(
        widget= forms.TextInput(
            attrs= {
                "class" : "txt-form form-control",
                "required"  : True,
                "placeholder"   : "Nama Lengkap",
            }
        )
    )
    notelepon = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form form-control",
                "required"  : True,
                "placeholder" : "No telepon",
            }
        )
    )
    namakonser = forms.CharField(
        widget= forms.TextInput(
            attrs = {
                "class" : "pilih-tanggal form-control",
                "required" : True,
                "placeholder" : "Nama Konser",
            }
        )

    )
    tempatkonser = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form form-control",
                "placeholder"  : "Tempat konser",
                "required"  : True,
            }
        )
    )
    tanggalkonser = forms.DateField(
        widget = forms.SelectDateWidget(
            attrs = {
                "class" : "pilih-tanggal form-control ",
                "required"  : True,
            }
        )
    )
    pesan = forms.CharField(
        widget = forms.Textarea(
            attrs = {
                "class" : "txt-form form-control",
                "placeholder" : "Pesan",
                "required"  : True,
                "rows":5, 
                "cols":10,
            }
        )
    )
    class Meta:
        model = models.KoncoKonser
        fields = [
            'namalengkap', 'notelepon', 'namakonser', 'tempatkonser', 'tanggalkonser', 'pesan',
        ]
