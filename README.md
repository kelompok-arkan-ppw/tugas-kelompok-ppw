# Tugas Kelompok PPW

Anggota-anggota kelompok:
1. Abdurrafi Arief - 1806205773
2. Arkan Sabilla Asfa - 1806191654
3. Michelle Melissa - 1806190891
4. Tesalonika - 1806191326

Nama Aplikasi: KoncoKonser
Link website: koncokonser.herokuapp.com

Aplikasi ini kegunaan untuk orang-orang yang ingin berangkat ke suatu konser tapi tidak memiliki teman untuk menemaninya ke konser. 
Aplikasi ini menyatukan orang-orang yang kebetulan ingin ke konser yang sama, menjadi ajang penghubung orang dan juga kesempatan mencari teman baru
dengan interests yang mirip.

Daftar Fitur:

Fitur: Cari konco untuk mencari teman yang ingin ke konser yang sama.
Fitur: Promote Event untuk mempromote suatu konser atau event
Fitur: Jual Merch untuk menjual merchendise based on idola atau artist.
Fitur: Jual Tiket untuk menjual tiket suatu event atau konser.

Pipeline status:
[![pipeline status](https://gitlab.com/kelompok-arkan-ppw/tugas-kelompok-ppw/badges/master/pipeline.svg)](https://gitlab.com/kelompok-arkan-ppw/tugas-kelompok-ppw/commits/master)



test code coverage: 1%