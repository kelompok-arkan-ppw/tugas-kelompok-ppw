from django.shortcuts import render,redirect

# Create your views here.
from .models import Tiket
from .forms import tiketForm

def viewjualtiket(request):
    return render(request, 'JualTiket.html')

def isijualtiket(request):
    form_jualtiket = tiketForm()

    if request.method == 'POST':
        form_jualtiket = tiketForm(request.POST)
        print(form_jualtiket.errors)
        if form_jualtiket.is_valid():
            form_jualtiket.save()
            return redirect('jualtiket:viewjualtiket')

    context = {
        'form_jualtiket':form_jualtiket,
    }

    return render(request, 'IsiJualTiket.html', {'form_jualtiket':form_jualtiket})

def viewjualtiket(request):
    list_jualtiket = Tiket.objects.all()

    context = {
        'list_jualtiket':list_jualtiket,
    }
    return render(request, 'JualTiket.html',context)
