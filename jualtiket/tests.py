from django.test import TestCase, Client
from django.urls import resolve
from .views import viewjualtiket, isijualtiket
from .forms import tiketForm

# Create your tests here.


# Create your tests here.
class TestViewJualTiket (TestCase):
    def test_findKonco_is_exist(self):
        response = Client().get('/viewjualtiket/')
        self.assertEqual(response.status_code,200)

    def test_viewjualtiket_using_html_template(self):
        response = Client().get('/viewjualtiket/')
        self.assertTemplateUsed(response, 'JualTiket.html')

    def test_viewjualtiket_using_index_func(self):
        found = resolve('/viewjualtiket/')
        self.assertEqual(found.func, index)


class TestFormJualTiket (TestCase):
    def test_formJualTiket_is_exist(self):
        response = Client().get('/isijualtiket/')
        self.assertEqual(response.status_code,200)

    def test_formJualTiket_using_html_template(self):
        response = Client().get('/isijualtiket/')
        self.assertTemplateUsed(response, 'formFindKonco.html')

    def test_formJualTiket_using_index_func(self):
        found = resolve('/isijualtiket/')
        self.assertEqual(found.func, index)
