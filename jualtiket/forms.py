from django import forms

from .models import Tiket

class tiketForm(forms.ModelForm):

    namaKonser = forms.CharField(
    widget= forms.TextInput(
        attrs= {
            "class" : "txt-form form-control",
            "required"  : True,
            "placeholder"   : "Nama Konser",
            }
        )
    )

    jenisTiket = forms.CharField(
    widget= forms.TextInput(
        attrs= {
            "class" : "txt-form form-control",
            "required"  : True,
            "placeholder"   : "Kategori",
            }
        )
    )

    tempat = forms.CharField(
    widget= forms.TextInput(
        attrs= {
            "class" : "txt-form form-control",
            "required"  : True,
            "placeholder"   : "Tempat Konser",
            }
        )
    )

    tanggal = forms.DateField(
    widget = forms.SelectDateWidget(
        attrs = {
            "class" : "pilih-tanggal form-control ",
            "required"  : True,
            }
        )
    )

    namaPenjual = forms.CharField(
    widget= forms.TextInput(
        attrs= {
            "class" : "txt-form form-control",
            "required"  : True,
            "placeholder"   : "Nama Penjual",
            }
        )
    )

    nomorTelepon = forms.CharField(
    widget= forms.TextInput(
        attrs= {
            "class" : "txt-form form-control",
            "required"  : True,
            "placeholder"   : "Nomor Telepon",
            }
        )
    )

    fotoTiket = forms.CharField(
    widget= forms.TextInput(
        attrs= {
            "class" : "txt-form form-control",
            "required"  : True,
            "placeholder"   : "Insert Your URL",
            }
        )
    )

    alasan = forms.CharField(
    widget= forms.TextInput(
        attrs= {
            "class" : "txt-form form-control",
            "required"  : True,
            "placeholder"   : "Alasan",
            }
        )
    )

    class Meta:
        model = Tiket
        fields = {
            'namaKonser',
            'jenisTiket',
            'tempat',
            'tanggal',
            'namaPenjual',
            'nomorTelepon',
            'fotoTiket',
            'alasan',
        }
