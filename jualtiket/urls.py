from django.urls import path,include

from . import views

app_name = 'jualtiket'

urlpatterns = [
   path('viewjualtiket/', views.viewjualtiket, name='viewjualtiket'),
   path('isijualtiket/', views.isijualtiket, name='isijualtiket'),
]
