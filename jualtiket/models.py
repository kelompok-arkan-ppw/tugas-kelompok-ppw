from django.db import models

# Create your models here.
class Tiket(models.Model):
    namaKonser = models.CharField(max_length=100)
    jenisTiket = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100)
    tanggal = models.DateField()
    namaPenjual = models.CharField(max_length=100)
    nomorTelepon = models.CharField(max_length=100)
    fotoTiket = models.CharField(max_length=255)
    alasan = models.CharField(max_length=255)


    def __str__(self):
        return "{}.{}".format(self.id,self.namaKonser,self.namaPenjual)
