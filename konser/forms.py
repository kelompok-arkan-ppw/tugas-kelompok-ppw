from . import models
from django import forms


class FormKonser(forms.ModelForm):
    namakonser = forms.CharField(
        widget= forms.TextInput(
            attrs= {
                "class" : "txt-form form-control",
                "required"  : True,
                "placeholder" : "Nama Konser",
            }
        )
    )
    instagram = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form form-control",
                "required"  : True,
                "placeholder" : "ID Instagram",
            }
        )
    )
    twitter = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form form-control",
                "required"  : True,
                "placeholder" : "ID Twitter",
            }
        )
    )
    tempatkonser = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form form-control",
                "placeholder"  : "Tempat konser",
                "required"  : True,
            }
        )
    )
    tanggalkonser = forms.DateField(
        widget = forms.SelectDateWidget(
            attrs = {
                "class" : "pilih-tanggal form-control ",
                "required"  : True,
            }
        )
    )
    poster = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form form-control",
                "placeholder" : "Link",
                "required"  : True,
            }
        )
    )
    pesan = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form form-control",
                "placeholder" : "Pesan",
                "required"  : True,
            }
        )
    )
    class Meta:
        model = models.EventKonser
        fields = [
            'namakonser', 'instagram', 'twitter', 'tempatkonser', 'tanggalkonser', 'poster', 'pesan',
        ]
