from django.shortcuts import render, redirect
from . import forms
from .models import EventKonser

# Create your views here.
def konser(request):
    return render(request, 'konser.html')

def formkonser(request):
    form = forms.FormKonser()
    if request.method == "POST":
        form = forms.FormKonser(request.POST)
        if form.is_valid():
            form.save()
            return redirect('konser:myformkonser')
    return render(request, 'formkonser.html', {'form':form})

def konser(request):
    daftar_item = EventKonser.objects.order_by('tanggalkonser')
    response = {
        'daftar_item' :daftar_item,
    }
    return render(request, 'konser.html', response)

def upload(request):
    return render(request, 'formkonser.html')