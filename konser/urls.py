from django.urls import path
from . import views

app_name = 'konser'

urlpatterns = [
    path('mykonser', views.konser, name='mykonser'),
    path('formkonser/', views.formkonser, name='myformkonser'),
]