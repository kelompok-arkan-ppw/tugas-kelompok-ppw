from django.urls import path
from .views import *

app_name = "jualmerch"

urlpatterns = [
    path('', landing),
    path('landing', landing, name='landing'),
    path('merchForm', merchForm, name='merchForm')
]