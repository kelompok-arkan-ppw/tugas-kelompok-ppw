from django.shortcuts import render, redirect, HttpResponseRedirect
from django.http import HttpResponse

from .forms import MerchForm
from .models import Merch

# Create your views here.

def landing(request):
    concertData = Merch.objects.all()
    
    context ={
        'CONTENT': concertData
    }

    return render(request, "JualMerch.html", context)


def merchForm(request):
    form = MerchForm()

    if request.method == "POST":
        form = MerchForm(request.POST)
        print(form.errors)
        if form.is_valid():
            
            Merch.objects.create(
                merchCategory = request.POST["merchCategoryForm"],
                merchName = request.POST["merchNameForm"],
                merchPrice = request.POST["merchPriceForm"],
                merchImage = request.POST["merchImageForm"],
                merchDescription= request.POST["merchDescriptionForm"],
                concertName = request.POST["concertNameForm"]
            )


            return HttpResponseRedirect("landing")
    context = {
        'form': form
    }
    
    return render(request, "MerchForm.html", context)



