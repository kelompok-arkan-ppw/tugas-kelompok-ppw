from django import forms

from .models import Merch


class MerchForm(forms.Form):
    merchNameForm = forms.CharField(label = "name")
    merchCategoryForm = forms.CharField(label = "category")
    merchPriceForm = forms.IntegerField(label = "price")
    merchImageForm = forms.CharField(label = "imageUrl")
    merchDescriptionForm = forms.CharField(label = "description")
    concertNameForm = forms.CharField(label = "concert name")
    

    

