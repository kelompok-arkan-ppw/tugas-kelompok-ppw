from django.db import models

# Create your models here.


class Merch(models.Model):
    merchName = models.CharField(max_length=100)
    merchCategory = models.CharField(max_length=100)
    merchPrice = models.IntegerField(default=0)
    merchImage = models.CharField(max_length = 500)
    merchDescription = models.CharField(max_length = 500)
    concertName = models.CharField(max_length=500)

    def __str__(self):
        return self.merchName

    def price(self):
        return "Rp. " + "%.2f" %(self.merchPrice)
    


